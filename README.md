# The Respect Scraper

## Overview
This app keeps track of how many times people comment 'F' on a post to the
["https://old.reddit.com/r/memes/"](r/memes) subreddit.
Commenting 'F' on a post is a sign of (ironic) respect on Reddit.
["https://knowyourmeme.com/memes/press-f-to-pay-respects"](Learn more...)

## Disclaimer
The posts this app scrapes from Reddit do not reflect my views or values. Some posts
catalogued may be inappropriate or offensive. If you find a post that you think
shouldn't be displayed on this site, don't hesitate to contact me at xxarcajethxx@gmail.com and I'll remove it.
