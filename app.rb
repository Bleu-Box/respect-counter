require "sinatra"
require "sqlite3"
require_relative "respect_scraper.rb"

# Run the scraper in a separate thread.
Thread.new do
  RespectScraper::run_scraper(subreddit: "r/memes", db_name: "respects.db")
end

get "/" do
  db = SQLite3::Database::new "respects.db" 
  rows = Array.new 

  # Load a list of posts
  db.execute "SELECT * FROM respects" do |row| 
    title, permalink, respects = row 
    rows << {title: title, permalink: permalink, respects: respects} 
  end 

  # Sort the posts based on their number of respects and take the top 100
  rows = rows.sort{|x, y| y[:respects] <=> x[:respects]}.take 100

  erb :home, locals: {rows: rows}
end

get "/about" do
  erb :about
end
