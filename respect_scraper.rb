# THE RESPECT SCRAPER
# ===================
# This script reads in Reddit comments (from r/memes) and counts
# how many times somebody comments 'F' (to pay respects) on
# each post. (https://knowyourmeme.com/memes/press-f-to-pay-respects)

require "json"
require "open-uri"
require "sqlite3"

module RespectScraper
  def self.run_scraper(subreddit:, db_name:)
    # The UTC timestamp of the most recent comment scraped in the last iteration.
    # This is used to ensure that we don't log comments that were already logged.
    last_scraped_utc = 0
    # A connection to the database
    db = SQLite3::Database::new db_name
    # The URI to receive JSON data from
    comments_uri = "https://reddit.com/#{subreddit}/comments.json?limit=100"
    
    loop do
      begin
        # Try to fetch the maximum number (100) of new comments on r/memes.
        # This may fail with HTTP error 429 if Reddit's server is too busy. 
        open comments_uri do |response|
          comments = self.parse_comments response.read
          comments.each do |comment|
            post_title = comment["link_title"]
            permalink = comment["link_permalink"]
            comment_utc = comment["created_utc"]
            comment_body = comment["body"]
            # If the comment is one we haven't already looked at
            # before in a previous iteration, see if it is "F" --
            # if so, record it. 
            # NOTE: So many people comment on Reddit at once that the scraper doesn't catch
            # all comments. Limiting this scraper's scope to r/memes is an attempt to remedy this,
            # but some respects will likely still sneak through without being counted.
            if comment_utc > last_scraped_utc && comment_body.upcase == "F"
              # See if the post is in our database already
              is_this_post = "permalink=\"#{permalink}\""              
              same_posts = db.execute "SELECT EXISTS(SELECT 1 FROM respects WHERE #{is_this_post})"
              
              # If the post is already in our database, increment the number of respects
              if same_posts != [[0]]
                db.execute "UPDATE respects SET num_respects = num_respects + 1 WHERE #{is_this_post}"
              else
                # Otherwise (if the post isn't in the database yet) put it in with the respect count at 1
                db.execute "INSERT INTO respects (post_title,permalink,num_respects) VALUES (?,?,?)",
                           [post_title, permalink, 1]
              end
            end
          end

          # Save the posting time of the most recent comment we scraped
          last_scraped_utc = comments[0]["created_utc"].to_i
        end
      rescue OpenURI::HTTPError => err
      # Do nothing, because these errors happen very frequently.
      rescue => err
        # If we encounter some other sort of error, print it.
        puts "*** Encountered #{err.class}: #{err}!"
        puts "*** Trying to recover..."
      end

      # wait a little bit
      # sleep 2
    end
  end

  private

  def self.parse_comments json
    comment_hash = JSON.parse json
    comment_hash["data"]["children"].map {|c| c["data"] }          
  end
end
